import pandas as pd
from xml.etree.ElementTree import Element, SubElement, ElementTree, tostring
from xml.dom import minidom
import os

def prettify(elem):
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")

# Get the directory where the script is located
script_directory = os.path.dirname(os.path.abspath(__file__))

# Load the CSV file into a DataFrame
csv_path = os.path.join(script_directory, 'UDT_req.csv')
df = pd.read_csv(csv_path)

# Group by UDT name ('Me' column)
grouped = df.groupby('UDT')

# Create a directory to save the generated L5X files
output_dir = os.path.join(script_directory, 'L5X_Files')
os.makedirs(output_dir, exist_ok=True)

# Loop through each UDT group to generate L5X file
for udt_name, group in grouped:
    if pd.isna(udt_name):
        continue
    
    root = Element('RSLogix5000Content', SchemaRevision="1.0", SoftwareRevision="34.01", 
                   TargetName=udt_name, TargetType="DataType", ContainsContext="true",
                   ExportDate="Fri Jun 30 10:47:36 2023", ExportOptions="References NoRawData L5KData DecoratedData Context Dependencies ForceProtectedEncoding AllProjDocTrans")
    controller = SubElement(root, 'Controller', Use="Context", Name="New")
    datatypes = SubElement(controller, 'DataTypes', Use="Context")
    datatype = SubElement(datatypes, 'DataType', Use="Target", Name=udt_name, Family="NoFamily", Class="User")
    members = SubElement(datatype, 'Members')
    
    for idx, row in group.iterrows():
        member_name = str(row['Member Name']) if not pd.isna(row['Member Name']) else ''
        member_datatype = str(row['Member DataType']) if not pd.isna(row['Member DataType']) else ''
        member_dimension = str(int(row['Member Dimension'])) if not pd.isna(row['Member Dimension']) else "0"
        member_external_access = str(row['Member External Access']) if not pd.isna(row['Member External Access']) else ''
        
        member = SubElement(members, 'Member', Name=member_name, DataType=member_datatype,
                            Dimension=member_dimension, Hidden="false", ExternalAccess=member_external_access)
        
        if not pd.isna(row['Description']):
            description = SubElement(member, 'Description')
            description.text = str(row['Description'])
    
    xml_str = prettify(root)
    
    output_file_path = os.path.join(output_dir, f"{udt_name}.L5X")
    with open(output_file_path, 'w') as f:
        f.write(xml_str)
