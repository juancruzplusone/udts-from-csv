# Notes:
# No "-" allowed in names
# No spaces allowed in names
# No "." allowed in names
import pandas as pd
from xml.etree.ElementTree import Element, SubElement, ElementTree, tostring
from xml.dom import minidom
import os

def prettify(elem):
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")

# Get the directory where the script is located
script_directory = os.path.dirname(os.path.abspath(__file__))

# Load the CSV file into a DataFrame
csv_path = os.path.join(script_directory, 'UDT_req.csv')
df = pd.read_csv(csv_path)

# Forward fill NaN values in the 'UDT' column
df['UDT'].fillna(method='ffill', inplace=True)

# Group by UDT name ('UDT' column)
grouped = df.groupby('UDT')

# Create a directory to save the generated L5X files
output_dir = os.path.join(script_directory, 'L5X_Files')
os.makedirs(output_dir, exist_ok=True)

# Create root element and its children for the Master L5X file
master_root = Element('RSLogix5000Content', SchemaRevision="1.0", SoftwareRevision="34.01", 
                      TargetName="UDT_Master", TargetType="DataType", ContainsContext="true",
                      ExportDate="Fri Jun 30 10:47:36 2023", ExportOptions="References NoRawData L5KData DecoratedData Context Dependencies ForceProtectedEncoding AllProjDocTrans")
master_controller = SubElement(master_root, 'Controller', Use="Context", Name="New")
master_datatypes = SubElement(master_controller, 'DataTypes', Use="Context")

# Loop through each UDT group to populate the Master L5X file
for udt_name, group in grouped:
    if pd.isna(udt_name):
        continue

    datatype = SubElement(master_datatypes, 'DataType', Use="Target", Name=udt_name, Family="NoFamily", Class="User")
    members = SubElement(datatype, 'Members')

    for idx, row in group.iterrows():
        member_name = str(row['Member Name']) if not pd.isna(row['Member Name']) else ''
        member_datatype = str(row['Member DataType']) if not pd.isna(row['Member DataType']) else ''
        member_dimension = str(int(row['Member Dimension'])) if not pd.isna(row['Member Dimension']) else "0"
        member_external_access = str(row['Member External Access']) if not pd.isna(row['Member External Access']) else ''
        
        member = SubElement(members, 'Member', Name=member_name, DataType=member_datatype,
                            Dimension=member_dimension, Hidden="false", ExternalAccess=member_external_access)
        
        # Add description if available
        if not pd.isna(row['Description']):
            description = SubElement(member, 'Description')
            description.text = f"<![CDATA[{str(row['Description'])}]]>"

# After finishing the loop for adding Data Types, we add the Dependencies section
dependencies = SubElement(master_datatypes, 'Dependencies')

# Loop through each UDT name to add them as dependencies
for udt_name in grouped.groups.keys():
    if pd.isna(udt_name):
        continue
    dependency = SubElement(dependencies, 'Dependency', Type="DataType", Name=udt_name)


# Generate pretty-printed XML string for the Master L5X file
master_xml_str = prettify(master_root)

# Post-process the XML string to replace pseudo-CDATA markers with actual CDATA sections
master_xml_str = master_xml_str.replace("&lt;![CDATA[", "<![CDATA[").replace("]]&gt;", "]]>")

# Save to Master L5X file
master_output_file_path = os.path.join(output_dir, "UDT_Master.L5X")
with open(master_output_file_path, 'w') as f:
    f.write(master_xml_str)