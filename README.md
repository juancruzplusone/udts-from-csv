# UDTs from CSV

## Description

This Python script converts User-Defined Types (UDTs) specified in a CSV file to L5X (XML) format compatible with Logix5000.

For easy importing into Studio 5000, it is recommended to use (master_udt_generator.py)[master_udt_generator.py]. This script will generate a single L5X file containing all UDTs specified in the CSV file.

If needed there is also a (individual_udt_generator.py)[individual_udt_generator.py] script that will generate a separate L5X file for each UDT specified in the CSV file. This script is not recommended for use with Studio 5000 as it will require each UDT to be imported individually. (Which I found out after I tried importing.)

## Requirements

- Python 3.x
- pandas library

## Usage

1. Place your CSV file in the same directory as the script. The CSV should have the filename `UDT_req.csv`. Alterntively, you can specify a different filename by changing the `csv_filename` variable in the script.

2. The CSV file should have the following columns:
    - UDT
    - Member Name
    - Member DataType
    - Member Dimension
    - Member External Access
    - Description

3. Run the script:
    ```bash
    python master_udt_generator.py
    ```

4. The L5X files will be generated in a folder named `L5X_Files` within the script's directory.

## Formatting Guide

### Important Notes

- No "-" allowed in names
- No spaces allowed in names
- No "." allowed in names

### CSV File Structure

| UDT          | Member Name  | Member DataType | Member Dimension | Member External Access | Description        |
|--------------|-------------|-----------------|------------------|------------------------|--------------------|
| Version      | PLC_Version  | STRING          |                  | Read Only              |                    |
| Datetime     | Year         | DINT            |                  | Read Only              | All times in UTC   |


Script to generate a master UDT from a CSV file in working directory.
